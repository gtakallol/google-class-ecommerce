<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
		
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
		
		

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape('logoinpass'.rand()) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', safe='0', date_added = NOW()");

		$customer_id = $this->db->getLastId();
		$customer_group_info['approval']=1;
		if ($customer_group_info['approval']) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_approval` SET customer_id = '" . (int)$customer_id . "', type = 'customer', date_added = NOW()");
		}
		$api=md5(rand(1000,100000));
		$random=rand(10,1000);
		$APIKey=md5($random."infoAPIKEY007".$api);
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "api` SET  info_id = '" . (int)$customer_id . "', account_type = '".(int)$data['account_type_id']."', status = '1',  key = '".$APIKey."', random = '".(int)$random."', date_added = NOW(), date_modified = NOW()");
		$api_id = $this->db->getLastId();
		
		$retrundata['id']=$api_id;
		$retrundata['apiran']=$api;
		$retrundata['ran']=$random;
		$retrundata['key']=$APIKey;
		
		
		
		return $retrundata;
	}
	public function addNewStudent($data) {
		
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
		
		

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', 
		customer_id = '" . (int)$data['customer_id'] . "', 
		firstname = '" . $this->db->escape($data['firstname']) . "', 
		lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape('logoinpass'.rand()) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', safe='0', date_added = NOW()");

		$customer_id = (int)$data['customer_id'];
		
		$customer_group_info['approval']=1;
		if ($customer_group_info['approval']) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_approval` SET customer_id = '" . (int)$customer_id . "', type = 'customer', date_added = NOW()");
		}
		$api=md5(rand(1000,100000));
		$random=rand(10,1000);
		$APIKey=md5($random."infoAPIKEY007".$api);
		
		$this->db->query("delete from  `" . DB_PREFIX . "api` where   info_id = '" . (int)$customer_id . "' and  account_type = '".(int)$data['account_type_id']."'");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "api` SET  info_id = '" . (int)$customer_id . "', account_type = '".(int)$data['account_type_id']."', status = '1',  key = '".$APIKey."', random = '".(int)$random."', date_added = NOW(), date_modified = NOW()");
		$api_id = $this->db->getLastId();
		
		$retrundata['id']=$api_id;
		$retrundata['apiran']=$api;
		$retrundata['ran']=$random;
		$retrundata['key']=$APIKey;
		
		
		return $retrundata;
	}
	
	public function addUser($data) {
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "user` SET user_id = '" . (int)$data['user_id'] . "',username = '" . $this->db->escape($data['email']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1(rand())) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', image = '', status = '1', date_added = NOW()");
		
		$user_id = (int)$data['user_id'];
		
		
		$api=md5(rand(1000,100000));
		$random=rand(10,1000);
		$APIKey=md5($random."infoAPIKEY007".$api);
		
		$this->db->query("delete from  `" . DB_PREFIX . "api` where   info_id = '" . (int)$user_id . "' and  account_type = '".(int)$data['account_type_id']."'");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "api` SET  info_id = '" . (int)$user_id . "', account_type = '".(int)$data['account_type_id']."', status = '1',  key = '".$APIKey."', random = '".(int)$random."', date_added = NOW(), date_modified = NOW()");
		$api_id = $this->db->getLastId();
		
		$retrundata['id']=$api_id;
		$retrundata['apiran']=$api;
		$retrundata['ran']=$random;
		$retrundata['key']=$APIKey;
		
		return $retrundata;
	}

	public function editUser($user_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['email']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', image = '' WHERE user_id = '" . (int)$user_id . "'");

		$api=md5(rand(1000,100000));
		$random=rand(10,1000);
		$APIKey=md5($random."infoAPIKEY007".$api);
		
		$this->db->query("delete from  `" . DB_PREFIX . "api` where   info_id = '" . (int)$user_id . "' and  account_type = '".(int)$data['account_type_id']."'");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "api` SET  info_id = '" . (int)$user_id . "', account_type = '".(int)$data['account_type_id']."', status = '1',  key = '".$APIKey."', random = '".(int)$random."', date_added = NOW(), date_modified = NOW()");
		$api_id = $this->db->getLastId();
		
		$retrundata['id']=$api_id;
		$retrundata['apiran']=$api;
		$retrundata['ran']=$random;
		$retrundata['key']=$APIKey;
		return $retrundata;
		
	}
	public function getUserByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "user` WHERE lower(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function editCustomer($customer_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '' WHERE customer_id = '" . (int)$customer_id . "'");
		
		
	
		
	}
	public function updateCustomer($customer_id, $data) {
		$api=md5(rand(1000,100000));
		$random=rand(10,1000);
		$APIKey=md5($random."infoAPIKEY007".$api);
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '' WHERE  customer_id = '" . (int)$customer_id . "'");
		
		$this->db->query("delete from  `" . DB_PREFIX . "api` where   info_id = '" . (int)$customer_id . "' and  account_type = '".(int)$data['account_type_id']."'");
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "api` SET  info_id = '" . (int)$customer_id . "', account_type = '".(int)$data['account_type_id']."', status = '1',  key = '".$APIKey."', random = '".(int)$random."', date_added = NOW(), date_modified = NOW()");
		$api_id = $this->db->getLastId();
		$retrundata['id']=$api_id;
		$retrundata['apiran']=$api;
		$retrundata['ran']=$random;
		$retrundata['key']=$APIKey;
		return $retrundata;
		
	}

	public function editPassword($email, $password) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editAddressId($customer_id, $address_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}
	
	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}

	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname, email FROM `" . DB_PREFIX . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}
	
	public function getCustomerByKey($data) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "api  WHERE 
		key = '" . $this->db->escape($data['key']) . "' and status=1 and used=1 and 
		random = '" . $this->db->escape($data['ran']) . "' and api_id = '" . $this->db->escape($data['id']) . "' ");
		if(isset($query->row['account_type'])){
			if($query->row['account_type']==2 || $query->row['account_type']==3){
				
			$customer_id=$query->row['info_id'];
			
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer  WHERE customer_id = '" . (int)$customer_id . "' ");
			return $query->row;
			
			}
		}
	}
	
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function addTransaction($customer_id, $description, $amount = '', $order_id = 0) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (float)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");
	}

	public function deleteTransactionByOrderId($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalTransactionsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}
	
	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
	
	public function addAffiliate($customer_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_affiliate SET `customer_id` = '" . (int)$customer_id . "', `company` = '" . $this->db->escape($data['company']) . "', `website` = '" . $this->db->escape($data['website']) . "', `tracking` = '" . $this->db->escape(token(64)) . "', `commission` = '" . (float)$this->config->get('config_affiliate_commission') . "', `tax` = '" . $this->db->escape($data['tax']) . "', `payment` = '" . $this->db->escape($data['payment']) . "', `cheque` = '" . $this->db->escape($data['cheque']) . "', `paypal` = '" . $this->db->escape($data['paypal']) . "', `bank_name` = '" . $this->db->escape($data['bank_name']) . "', `bank_branch_number` = '" . $this->db->escape($data['bank_branch_number']) . "', `bank_swift_code` = '" . $this->db->escape($data['bank_swift_code']) . "', `bank_account_name` = '" . $this->db->escape($data['bank_account_name']) . "', `bank_account_number` = '" . $this->db->escape($data['bank_account_number']) . "', `status` = '" . (int)!$this->config->get('config_affiliate_approval') . "'");
		
		if ($this->config->get('config_affiliate_approval')) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_approval` SET customer_id = '" . (int)$customer_id . "', type = 'affiliate', date_added = NOW()");
		}		
	}
		
	public function editAffiliate($customer_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer_affiliate SET `company` = '" . $this->db->escape($data['company']) . "', `website` = '" . $this->db->escape($data['website']) . "', `commission` = '" . (float)$this->config->get('config_affiliate_commission') . "', `tax` = '" . $this->db->escape($data['tax']) . "', `payment` = '" . $this->db->escape($data['payment']) . "', `cheque` = '" . $this->db->escape($data['cheque']) . "', `paypal` = '" . $this->db->escape($data['paypal']) . "', `bank_name` = '" . $this->db->escape($data['bank_name']) . "', `bank_branch_number` = '" . $this->db->escape($data['bank_branch_number']) . "', `bank_swift_code` = '" . $this->db->escape($data['bank_swift_code']) . "', `bank_account_name` = '" . $this->db->escape($data['bank_account_name']) . "', `bank_account_number` = '" . $this->db->escape($data['bank_account_number']) . "' WHERE `customer_id` = '" . (int)$customer_id . "'");
	}

	
	public function getAffiliate($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_affiliate` WHERE `customer_id` = '" . (int)$customer_id . "'");

		return $query->row;
	}
	
	public function getPoints($customer_id) {
		$query = $this->db->query("SELECT points FROM `" . DB_PREFIX . "customer` WHERE `customer_id` = '" . (int)$customer_id . "'");

		return $query->row['points'];
	}
	
	public function getAffiliateByTracking($tracking) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_affiliate` WHERE `tracking` = '" . $this->db->escape($tracking) . "'");

		return $query->row;
	}			
}