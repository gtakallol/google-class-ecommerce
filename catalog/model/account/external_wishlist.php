<?php
class ModelAccountExternalWishlist extends Model {
	public function getTransactions($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "external_wishlist` WHERE customer_id = '" . (int)$this->customer->getId() . "'";

		$sort_data = array(
			'sort_order',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if(DB_DRIVER=='pgsql') $sql .= " LIMIT " . (int)$data['start'] . " OFFSET " . (int)$data['limit']; 	else $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getExternalWishlist($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "external_wishlist` WHERE customer_id = '" . (int)$this->customer->getId() . "'";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalTransactions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "external_wishlist` WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}

	public function getTotalAmount() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM `" . DB_PREFIX . "customer_transaction` WHERE customer_id = '" . (int)$this->customer->getId() . "' GROUP BY customer_id");

		if ($query->num_rows) {
			return $query->row['total'];
		} else {
			return 0;
		}
	}
	
	public function addWishlist($customer_id, $data){  //echo '<pre>'; print_r($data); exit;

		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "external_wishlist SET customer_id = '" . (int)$customer_id. "', name = '" . $this->db->escape($product_image['name']) . "', sort_order = '" . (int)$product_image['sort_order'] . "', link = '" . $product_image['link'] . "', date_added = NOW()");
		     }
	    }
	}
	public function editWishlist($customer_id, $data){   
		$this->db->query("DELETE FROM " . DB_PREFIX . "external_wishlist WHERE customer_id = '" . (int)$customer_id . "'");

		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "external_wishlist SET customer_id = '" . (int)$customer_id. "', name = '" . $this->db->escape($product_image['name']) . "', sort_order = '" . (int)$product_image['sort_order'] . "', link = '" . $product_image['link'] . "', date_added = NOW()");
		     }
	    }
	}
}