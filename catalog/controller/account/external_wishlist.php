<?php
class ControllerAccountExternalWishlist extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/external_wishlist', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/external_wishlist');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_transaction'),
			'href' => $this->url->link('account/external_wishlist', '', true)
		);

		$this->load->model('account/external_wishlist');
		
		$data['column_amount'] = sprintf($this->language->get('column_amount'), $this->config->get('config_currency'));

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['transactions'] = array();

		$filter_data = array(
			'sort'  => 'date_added',
			'order' => 'DESC',
			'start' => ($page - 1) * 10,
			'limit' => 10
		);

		$transaction_total = $this->model_account_external_wishlist->getTotalTransactions();

		$results = $this->model_account_external_wishlist->getTransactions($filter_data);

		foreach ($results as $result) {
			$data['transactions'][] = array(
				'id'      => $result['id'],
				'description' => $result['link'],
				'short_description' =>  substr($result['link'], 0, 100) ,
				'name' => $result['name'],
				'date_added'  => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $transaction_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/external_wishlist', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($transaction_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($transaction_total - 10)) ? $transaction_total : ((($page - 1) * 10) + 10), $transaction_total, ceil($transaction_total / 10));

		$data['total'] = $this->currency->format($this->customer->getBalance(), $this->session->data['currency']);

		$data['add'] = $this->url->link('account/external_wishlist/add', '', true);
		$data['edit'] = $this->url->link('account/external_wishlist/edit', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/external_wishlist', $data));
	}
    public function add() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/affiliate', '', true);

			$this->response->redirect($this->url->link('affiliate/login', '', true));
		}

		$this->load->language('account/external_wishlist');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/external_wishlist');

		//if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_account_external_wishlist->addWishlist($this->customer->getId(), $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/external_wishlist', '', true));
		}
		
		$this->getForm();
	}
	
	public function edit() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/affiliate', '', true);

			$this->response->redirect($this->url->link('affiliate/login', '', true));
		}

		$this->load->language('account/external_wishlist');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/external_wishlist');

		//if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_account_external_wishlist->editWishlist($this->customer->getId(), $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/external_wishlist', '', true));
		}
		
		$this->getForm();
	}
		
	public function getForm() {
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/external_wishlist', '', true)
		);

		if ($this->request->get['route'] == 'account/external_wishlist/add') {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_affiliate'),
				'href' => $this->url->link('account/external_wishlist/add', '', true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_affiliate'),
				'href' => $this->url->link('account/external_wishlist/edit', '', true)
			);		
		}
	
		
 		$data['action'] = $this->url->link('account/external_wishlist/add' );
		
		$this->load->model('account/external_wishlist');
		$product_images = $this->model_account_external_wishlist->getExternalWishlist();
		
 		if (isset($this->request->post['product_image'])) {
			$product_images = $this->request->post['product_image'];
		} elseif (isset($product_images)) {		
			$product_images = $this->model_account_external_wishlist->getExternalWishlist();
		} else {
			$product_images = array();
		} 
		
		if (empty($product_images)) {
			$data['action'] = $this->url->link('account/external_wishlist/add' );
		} else {
			$data['action'] = $this->url->link('account/external_wishlist/edit' );
		}

		$data['product_images'] = array();

		foreach ($product_images as $product_image) {

			$data['product_images'][] = array(
				'name'         => $product_image['name'],
				'sort_order'   => $product_image['sort_order'],
				'link'         => $product_image['link'],
			);
		}

		
		$data['back'] = $this->url->link('account/external_wishlist', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/external_wishlist_form', $data));
	}
	
	protected function validate() {
/* 		if ($this->request->post['payment'] == 'cheque' && !$this->request->post['cheque']) {
			$this->error['cheque'] = $this->language->get('error_cheque');
		} elseif (($this->request->post['payment'] == 'paypal') && ((utf8_strlen($this->request->post['paypal']) > 96) || !filter_var($this->request->post['paypal'], FILTER_VALIDATE_EMAIL))) {
			$this->error['paypal'] = $this->language->get('error_paypal');
		} elseif ($this->request->post['payment'] == 'bank') {
			if ($this->request->post['bank_account_name'] == '') {
				$this->error['bank_account_name'] = $this->language->get('error_bank_account_name');
			}
	
			if ($this->request->post['bank_account_number'] == '') {
				$this->error['bank_account_number'] = $this->language->get('error_bank_account_number');
			}
		}
		
		// Custom field validation
		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

		foreach ($custom_fields as $custom_field) {
			if ($custom_field['location'] == 'affiliate') {
				if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				} elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
			}
		}			
		
		// Validate agree only if customer not already an affiliate
		$affiliate_info = $this->model_account_customer->getAffiliate($this->customer->getId());
				
		if (!$affiliate_info && $this->config->get('config_affiliate_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_affiliate_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		} */

		return !$this->error;
	}		
}