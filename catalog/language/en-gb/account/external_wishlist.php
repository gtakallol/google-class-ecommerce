<?php
// Heading
$_['heading_title']      = 'External Wishlist';

// Column
$_['column_name']        = 'Product name';
$_['column_description'] = 'Link';
$_['column_amount']      = 'Amount (%s)';

// Text
$_['text_account']       = 'Account';
$_['text_affiliate']     = 'External Wishlist';
$_['text_transaction']   = 'External Wishlist';
$_['text_total']         = 'Your current balance is:';
$_['text_empty']         = 'You do not have any wishlist!';

$_['button_add']         = 'Add';
$_['button_edit']        = 'Edit';
$_['button_save']        = 'Save';

$_['entry_sort_order']   = 'Sort order';
$_['entry_product_name'] = 'Product name';
$_['entry_link']         = 'Link';

