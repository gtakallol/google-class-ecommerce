<?php
// Heading
$_['heading_title']     = 'Student Wishlist';

// Text
$_['text_success']      = 'Success: You have modified student groups!';
$_['text_list']         = 'Student Wishlist';
$_['text_add']          = 'Add Student Group';
$_['text_edit']         = 'Edit Student Group';

// Column
$_['column_name']       = 'Student';
$_['column_sort_order'] = 'Product Name';
$_['column_action']     = 'URL';

// Entry
$_['entry_name']        = 'Student Group Name';
$_['entry_description'] = 'Description';
$_['entry_approval']    = 'Approve New Students ';
$_['entry_sort_order']  = 'Sort Order';

// Help
$_['help_approval']     = 'Students  must be approved by an administrator before they can login.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify student groups!';
$_['error_name']        = 'Student Group Name must be between 3 and 32 characters!';
$_['error_default']     = 'Warning: This student group cannot be deleted as it is currently assigned as the default store student group!';
$_['error_store']       = 'Warning: This student group cannot be deleted as it is currently assigned to %s stores!';
$_['error_customer']    = 'Warning: This student group cannot be deleted as it is currently assigned to %s customers!';