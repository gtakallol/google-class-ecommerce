<?php
// Heading
$_['heading_title']         = 'Student Approvals';

// Text
$_['text_success']          = 'Success: You have modified student approvals!';
$_['text_list']             = 'Student Approval List';
$_['text_default']          = 'Default';
$_['text_customer']         = 'Student ';
$_['text_affiliate']        = 'Affiliate';

// Column
$_['column_name']           = 'Student Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Student Group';
$_['column_type']           = 'Type';
$_['column_date_added']     = 'Date Added';
$_['column_action']         = 'Action';

// Entry
$_['entry_name']            = 'Student Name';
$_['entry_email']           = 'E-Mail';
$_['entry_customer_group']  = 'Student Group';
$_['entry_type']            = 'Type';
$_['entry_date_added']      = 'Date Added';

// Error
$_['error_permission']      = 'Warning: You do not have permission to modify student approvals!';