<?php
// Heading
$_['heading_title']         = 'Student Transaction Report';

// Column
$_['text_extension']        = 'Extensions';
$_['text_edit']             = 'Edit Student Transaction Report';
$_['text_success']          = 'Success: You have modified student credit report!';
$_['text_filter']           = 'Filter';

// Column
$_['column_customer']       = 'Student Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Student Group';
$_['column_status']         = 'Status';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';

// Entry
$_['entry_date_start']      = 'Date Start';
$_['entry_date_end']        = 'Date End';
$_['entry_customer']        = 'Student ';
$_['entry_status']          = 'Status';
$_['entry_sort_order']      = 'Sort Order';

// Error
$_['error_permission']      = 'Warning: You do not have permission to modify student credit report!';