<?php
// Heading
$_['heading_title']         = 'Student Orders Report';

// Text
$_['text_extension']        = 'Extensions';
$_['text_edit']             = 'Edit Student Orders Report';
$_['text_success']          = 'Success: You have modified student orders report!';
$_['text_filter']           = 'Filter';
$_['text_all_status']       = 'All Statuses';

// Column
$_['column_customer']       = 'Student Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Student Group';
$_['column_status']         = 'Status';
$_['column_orders']         = 'No. Orders';
$_['column_products']       = 'No. Products';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';

// Entry
$_['entry_date_start']      = 'Date Start';
$_['entry_date_end']        = 'Date End';
$_['entry_customer']        = 'Student ';
$_['entry_status']          = 'Order Status';
$_['entry_status']          = 'Status';
$_['entry_sort_order']      = 'Sort Order';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify student orders report!';