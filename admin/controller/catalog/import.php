<?php
class ControllerCatalogImport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/import');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/import');

		$this->getList();
	}



	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/import', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/import/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/import/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['action'] = $this->url->link('catalog/import/upload', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['informations'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$information_total = $this->model_catalog_import->getTotalInformations();

		$results = $this->model_catalog_import->getInformations($filter_data);

		foreach ($results as $result) {
			$data['informations'][] = array(
				'information_id' => $result['information_id'],
				'title'          => $result['title'],
				'sort_order'     => $result['sort_order'],
				'edit'           => $this->url->link('catalog/import/edit', 'user_token=' . $this->session->data['user_token'] . '&information_id=' . $result['information_id'] . $url, true)
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('catalog/im', 'user_token=' . $this->session->data['user_token'] . '&sort=id.title' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/import', 'user_token=' . $this->session->data['user_token'] . '&sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $information_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/import', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($information_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($information_total - $this->config->get('config_limit_admin'))) ? $information_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $information_total, ceil($information_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/import_list', $data));
	}


	
	
	public function upload(){
		
		require 'SimpleXLSX.php';
		$target_dir = DIR_IMAGE ;
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
	
	    $file_extention= pathinfo($_FILES["fileToUpload"]["name"]);
		
		if(!empty($_FILES["fileToUpload"]["name"])){
			if ( $xlsx = SimpleXLSX::parse($target_file) ) {
				//echo '<pre>'; print_r( $xlsx->rows()[0][3] ); 

				if (in_array('name',$xlsx->rows()[0])==false || in_array('category',$xlsx->rows()[0])==false) {

					$this->session->data['success'] = 'Please select file with wright format';
					$this->response->redirect($this->url->link('catalog/import', 'user_token=' . $this->session->data['user_token'], true));					
				} 
				
				$data = array();

				for($j=1;$j<count($xlsx->rows());$j++){
					foreach ($xlsx->rows()[0] as $x => $x_value) {

							$data[$x_value]= $xlsx->rows()[$j][$x];
 
					}

					$this->load->model('catalog/import');
					$data['category_id'] = $this->model_catalog_import->getCategoryId($data['category']);
					if(empty($data['category_id'])){
						$cat_data['category_description'][1] = array(
							'name' => $data['category'],
						);
						$cat_data['status'] = 1;
						$cat_data['parent_id'] = 0;
						
						$this->load->model('catalog/category');
						$category_id=$this->model_catalog_category->addCategory($cat_data); 
						$data['category_id'] = $category_id;
					}
					if (!empty($data['image'])){
						$url = $data['image'];
						$name = basename($url);
						file_put_contents(DIR_IMAGE."catalog/demo/$name", file_get_contents($url));
						$data['image']="catalog/demo/$name";
					}						
					$data['product_description'][1] = array(
						'name' => $data['name'],
						'description'     => $data['description'],
						'tag'     => $data['tag'],
					);
					$data['status'] = 1;
					$data['date_available'] = '0000-00-00';
					
					$this->load->model('catalog/product');
					$this->model_catalog_product->addProduct($data); 
					
					$this->load->language('catalog/import');
					$this->session->data['success'] = $this->language->get('text_success');
					
					$this->response->redirect($this->url->link('catalog/import', 'user_token=' . $this->session->data['user_token'] .'&success=success', true));
				}	
             
			} else {
					
					$data['header'] = $this->load->controller('common/header');
					$data['column_left'] = $this->load->controller('common/column_left');
					$data['footer'] = $this->load->controller('common/footer');
					$data['error_warning']= SimpleXLSX::parseError();
					$this->response->setOutput($this->load->view('catalog/import_list', $data));
			}
		}else{
			        $data['header'] = $this->load->controller('common/header');
					$data['column_left'] = $this->load->controller('common/column_left');
					$data['footer'] = $this->load->controller('common/footer');
					$data['error_warning']= 'Please select a file';
					$this->response->setOutput($this->load->view('catalog/import_list', $data));
		}	
	}
}
