<?php
namespace DB;
final class PgSQL {
	private $link;

	public function __construct($hostname, $username, $password, $database, $port = '5432') {
		//echo 'host=' . $hostname . ' port=' . $port .  ' user=' . $username . ' password='	. $password . ' dbname=' . $database;
		if (!$this->link = pg_connect('host=' . $hostname . ' port=' . $port .  ' user=' . $username . ' password='	. $password . ' dbname=' . $database)) {
			throw new \Exception('Error: Could not make a database link using ' . $username . '@' . $hostname);
		}

		//if (!mysql_select_db($database, $this->link)) {
			//throw new \Exception('Error: Could not connect to database ' . $database);
		//}

		pg_query($this->link, "SET CLIENT_ENCODING TO 'UTF8'");
	}

	public function query($sql) {
		$sql2=$sql;
		
		if(strpos($sql,'INSERT INTO' ) !== false && strpos($sql,' SET ' ) !== false){
			
				$explode=explode('SET',$sql);
				$explode222=explode(",",$explode['1']);
				$fields=array();
				$values=array();
				foreach($explode222 as $value){
					if(substr_count($value,'=')>1){
						$from='=';
						$from = '/'.preg_quote($from, '/').'/';

						$value=preg_replace($from, '@@@@@@@@@@@@@@@@@@', $value, 1);
						
						$info=explode('@@@@@@@@@@@@@@@@@@',$value);
						
						
					}else 
					$info=explode('=',$value);
					$fields[]=$info[0];
					$values[]=$info[1];
				}
			$sql= $explode[0]." (".implode(',',$fields).") VALUES ( ". implode(',', $values)." )";
			
		} else{
			//echo "Word Not Found!";
		}

		if(strpos($sql,' LIMIT ' ) !== false   && substr_count($sql,' LIMIT ')==1){
				//echo  substr_count($sql,' LIMIT '); exit;
				//&& substr_count(' LIMIT ',$sql)==1
				if( strpos($sql,'OFFSET' ) !== false){
					
				}else {
						$explode=explode(' LIMIT ',$sql);
						$explode[1]=str_replace(',','|',$explode[1]);
						$sql=$explode[0]." LIMIT ". $explode[1];
				}
			} else{
			//echo "Word Not Found!";
		}
		
		
		if(strpos($sql,' LIMIT ' ) !== false ){
				//echo  substr_count($sql,' LIMIT '); exit;
				//&& substr_count(' LIMIT ',$sql)==1
				if( strpos($sql,' OFFSET ' ) !== false){
					
					$sql =str_replace(' OFFSET ', '|',$sql);
					
				}
			} 

		

	
		//$sql =str_replace('NOW()', 'transaction_timestamp()',$sql);
		
		if(strpos($sql,'CREATE SEQUENCE' ) !== false){
				return null;
		} else{
			//echo "Word Not Found!";
		}



		$sql =str_replace('0000-00-00', '0001-01-01',$sql);
		$sql =str_replace('`column`', ' `column_category` ',$sql);
		$sql =str_replace('column int', ' column_category int ',$sql);
		$sql =str_replace('column,', ' column_category, ',$sql);
		
		$sql =str_replace('int NOT NULL DEFAULT NEXTVAL (',' SERIAL DEFAULT NEXTVAL (',$sql);
		
			//$string = "attribute_id int NOT NULL DEFAULT NEXTVAL ('oc_attribute_seq')";
		if(strpos($sql,'LCASE' ) !== false ){
			$sql=str_replace('LCASE(','LOWER(',$sql) ; 
		}		
		
		
		
		$sql= preg_replace("/DEFAULT NEXTVAL \([^)]+\)/","",$sql);
		

		$sql=str_replace('`',' ',$sql) ; 
		$_SESSION['last_inserted_database']='';
		if(strpos($sql,'INSERT INTO' ) !== false ){
			
			$words = explode(' ', $sql);
			
			foreach($words as $word) {
				if(strpos($word,DB_PREFIX ) !== false ){
					
					$_SESSION['last_inserted_database']=$word;
					
					
					break;
					
				}	
			}
			
			

		}	
		
		if(strpos($sql,'CREATE SEQUENCE' ) !== false){
				return null;
		} else{
			//echo "Word Not Found!";
		}

		$resource = pg_query($this->link, $sql);

		if ($resource) {
			if (is_resource($resource)) {
				$i = 0;

				$data = array();

				while ($result = pg_fetch_assoc($resource)) {
					$data[$i] = $result;

					$i++;
				}

				pg_free_result($resource);

				$query = new \stdClass();
				$query->row = isset($data[0]) ? $data[0] : array();
				$query->rows = $data;
				$query->num_rows = $i;

				unset($data);
				/*
				echo "<br />";
				echo "<br />";
				echo "<br />";
				echo $sql2;
				echo "<br />";
				echo "<br />";
				echo $sql;
				echo "<br />";
				echo "<br />";*/
				return $query;
			} else {
				return true;
			}
		} else {
				echo "<br />";
				echo "<br />";
				echo "<br />";
				echo $sql2;
				echo "<br />";
				echo "<br />";
				echo $sql;
				echo "<br />";
				echo "<br />";
			throw new \Exception('Error: ' . pg_result_error($this->link) . '<br />' . $sql);
		}
	}

	public function escape($value) {
		return pg_escape_string($this->link, $value);
	}

	public function countAffected() {
		return pg_affected_rows($this->link);
	}

	public function getLastId() {
		
		$databae_table=$_SESSION['last_inserted_database'];
		//$sql="SELECT currval(".str_replace('oc_','',$_SESSION['last_inserted_database'])."_id )from ".$_SESSION['last_inserted_database']."";
		//$sql="SELECT currval(".str_replace('oc_','',$_SESSION['last_inserted_database'])."_id)";
		
		//echo $sql="SELECT MAX(".str_replace('oc_','',$databae_table)."_id) as id from ".$databae_table.""; 
		//$query = $this->query($sql);
		//echo $query->row['id']."<br />";
		
		$sql="SELECT select currval('".$databae_table."_id_seq') as id "; 
		$sql="select currval(pg_get_serial_sequence('".$databae_table."', '".str_replace(DB_PREFIX,'',$databae_table)."_id')) as id "; 
		$query = $this->query($sql);
		
		//$query = $this->query($sql);
		

		return $query->row['id'];
	}

	public function __destruct() {
		pg_close($this->link);
	}
	public function rawSql($params, $table, $operation = "INSERT", $where = "")
    {
		
        $sql = "";
        $table = $table;
	
        $fields = [];
        $values = [];
		foreach($params as $key=>$value){
			 $fields[] = $key;
			  $values[] = $value;
			
		}
		
        if ($operation == "INSERT") {
            $sql = "INSERT INTO $table(" . implode(",", $fields) . ") VALUES (" . implode(",", $values) . ")";
            return $sql;
        }
        $values = [];
		foreach($params as $key=>$value){
			 $field= $this->sqlConvert($key);
			  $value = "'".$this->escape($value)."'";
			  $values[] = "$field=$value" ;
			
		}
     
        $sql = "UPDATE $table SET " . implode(",", $values) . " $where";
        return $sql;
    }
}